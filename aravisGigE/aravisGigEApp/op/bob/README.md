# aravisGigE is [obsolete](https://github.com/areaDetector/aravisGigE)!

These OPIs are provided for compatibility and historical reasons.

_Consider using [ADAravis](https://github.com/areaDetector/ADAravis) and [ADAravis OPIs](ADAravis/aravisApp/op/bob/) instead._
